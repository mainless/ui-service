#include "engine.h"
#include <glad/glad.h>

struct UIRenderer : Subscriber
{
    UIRenderer () : Subscriber(&EngineInstance->GameFrame) {}
    void Execute () override
    {
        static unsigned char a = 0;
        a++;
        glClearColor((a&1) * .1, (a&2) * .1, (a&4) * .1, 1);
        glClear(GL_COLOR_BUFFER_BIT);
        glFinish();
    	SDL_GL_SwapWindow(EngineInstance->WindowInstance.GameWindow);
    }
}
UIRendererInstance;
